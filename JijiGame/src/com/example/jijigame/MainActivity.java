package com.example.jijigame;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {
	
	Button button1;
	Button button2;
	Button button3;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		addListenerOnButton();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void addListenerOnButton() {
		 
		button1 = (Button) findViewById(R.id.button_check1);
 
		button1.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				Button buttonView = (Button)v;
				buttonView.setText("Beijing");
				
				v.setOnClickListener(null); 
			}
 
		});
		
		button2 = (Button) findViewById(R.id.button_check2);
		 
		button2.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				Button buttonView = (Button)v;
				buttonView.setText("Moscow");
				
				v.setOnClickListener(null); 
			}
 
		});
		
		button3 = (Button) findViewById(R.id.button_check3);
		 
		button3.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				Button buttonView = (Button)v;
				buttonView.setText("Paris");
				
				v.setOnClickListener(null); 
			}
 
		});
 
	}

}
